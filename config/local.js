module.exports = {
	host: process.env.OPENSHIFT_NODEJS_IP,
	port: process.env.OPENSHIFT_NODEJS_PORT || 1337,
	environment: process.env.NODE_ENV || 'production'
}