define([
  'angular',
  'sails.io',
  'ng-table',
  'uibootstrap',
  'controllers'
], function (angular, io, ngTable) {

  var socket = io.connect(), app;
  socket.on('connect', function socketConnected() {
    console.log('Socket is now connected');
  });

  app = angular.module('todoIt', [
     'controllers','ngTable', 'ui.bootstrap'
  ]);

  return app;

});