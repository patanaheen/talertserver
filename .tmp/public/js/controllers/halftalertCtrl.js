define(function () {
    return ['$scope', '$http', function($scope, $http) {

        $scope.location = '';
        $scope.city = '';
        
        $scope.htalertVerify = function (data) {
            $http.put('/api/halftalert/update/' + data.id , 
                {verified:((data.verified) ? false: true)}).success(function(){
                    $scope.halerts.splice(data.index, 1);
                });
        };

        $scope.addHtalert = function () {
            $http.put('/api/halftalert/create', {city:$scope.city,
                location:$scope.location
            }).success(function(data) {
              $scope.halerts.push(data);
            });
        };

        $scope.removeHtalert = function (data) {
            $http.put('/api/halftalert/update/' + data.id , 
                {deleted:((data.deleted) ? false: true)}).success(function(){
                    $scope.halerts.splice(data.index, 1);
                });
        };

        $http.get('/api/halftalert/find?where={"verified":false,"deleted":false}').success(function(data) {
          for (var i = 0; i < data.length; i++) {
            data[i].index = i;
          }
          $scope.halerts = data;
        });
    }];
});