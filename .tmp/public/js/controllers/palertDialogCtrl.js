
define(function () {
  return ['$scope', '$http', '$modalInstance', function($scope, $http, $modalInstance) {

    // $http.put('/api/policealert/create', {city:$scope.city,
    //     location:$scope.location
    // }).success(function(data) {
    //   $scope.halerts.push(data);
    // });
    $scope.city = '';
    $scope.location = '';
    $scope.theft = false;
    $scope.harrassment = false;
    $scope.murder = false;
    $scope.kidnapping = false;
    $scope.fraud = false;
    $scope.robbery = false;
    $scope.creditcardfraud = false;

    // $scope.dataModel = [
    //   {"colName": "city",
    //       "type": "string"
    //   },
    //   {"colName": "location",
    //       "type": "string"
    //   },
    //   {"colName": "theft",
    //       "type": "boolean"
    //   },
    //   {"colName": "harrassment",
    //       "type": "boolean"
    //   },
    //   {"colName": "murder",
    //       "type": "boolean"
    //   },
    //   {"colName": "kidnapping",
    //       "type": "boolean"
    //   },
    //   {"colName": "fraud",
    //       "type": "boolean"
    //   },
    //   {"colName": "robbery",
    //       "type": "boolean"
    //   },
    //   {"colName": "creditcardfraud",
    //       "type": "boolean"
    //   }
    // ];

    $scope.ok = function () {
      console.log($scope.city);
      console.log($scope.location);

      $http.put('/api/policealert/create', {city:$scope.city,
          location:$scope.location,
          theft:$scope.theft,
          harrassment:$scope.harrassment,
          murder:$scope.murder,
          kidnapping:$scope.kidnapping,
          fraud:$scope.fraud,
          robbery:$scope.robbery,
          creditcardfraud:$scope.creditcardfraud
        }).success(function(data) {
        $modalInstance.close(data);
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  }];
});
