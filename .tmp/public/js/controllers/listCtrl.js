// define(function () {
//     return ['$scope', function($scope) {
//         $scope.addItem = function () {
//             console.log('addItem');
//         };
//         $scope.removeItems = function () {
//             console.log('removeItems');
//         };
//         $scope.items = [{
//             description: 'Do a thing',
//             completed: true
//         }, {
//             description: 'Do another thing',
//             completed: false
//         }, {
//             description: 'Do a third thing',
//             completed: false
//         }];
//     }];
// });

define(function () {
    return ['$scope', '$http', function($scope, $http) {

        $scope.description = '';
        
        // $scope.addPhoto = function (data) {
        //     var f = document.getElementById('file').files[0];
        //     var r = new FileReader();
        //     r.onloadend = function(e){
        //         $http.put('/talert/uploadFile', {description:$scope.description} ).success(function(data) {
        //           // $scope.items.push(data);
        //           console.log("pushing to server")
        //         });
        //     }
        //     r.readAsBinaryString(f);
            

        //     $http.put('/talert/uploadFile', {description:$scope.description} ).success(function(data) {
        //       $scope.items.push(data);
        //     });
        // };

        $scope.itemCheck = function (data) {
            $http.put('/api/talert/update/' + data.id , {completed:((data.completed) ? false: true)});
        };

        $scope.addItem = function () {
            $http.put('/api/talert/create', {description:$scope.description} ).success(function(data) {
              $scope.items.push(data);
            });
        };

        $scope.removeItem = function (data) {
            $http['delete']('/api/talert/' + data.id).success(function() {
              $scope.items.splice(data.index, 1);
            });
        };

        $http.get('/api/talert/find').success(function(data) {
          for (var i = 0; i < data.length; i++) {
            data[i].index = i;
          }
          $scope.items = data;
        });
    }];
});