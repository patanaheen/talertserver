window.name = 'NG_DEFER_BOOTSTRAP!';

require.config({
  'baseUrl': '/js',
  'paths': {
    'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min',
    'angular': '//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular',
    'ng-table': '/js/ng-table',
    'ngResource': '/js/angular-resource',
    'bootstrap': '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min',
    'uibootstrap':'//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.1/ui-bootstrap-tpls.min',
    'ngDialog': '//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.3.7/js/ngDialog.min'

  },
  'shim': {
    'angular': {
      'exports': 'angular'
    },
    'bootstrap': {
      'deps': ['jquery']
    },
    'uibootstrap': {
      'deps': ['angular']
    },
    'socket.io': {
      'exports': 'io'
    },
    'ng-table': {
      'deps': ['jquery','angular'],
    },
    'sails.io': {
      'deps': ['socket.io'],
      'exports': 'io'
    }
  }
});

require([
  'angular',
  'app',
  'bootstrap'
], function (angular, app) {

  angular.element(document.getElementsByTagName('html')[0]);
  // angular.module('todoIt')
  // .directive('loadingContainer', function () {
  //     return {
  //         restrict: 'A',
  //         scope: false,
  //         link: function(scope, element, attrs) {
  //             var loadingLayer = angular.element('<div class="loading"></div>');
  //             element.append(loadingLayer);
  //             element.addClass('loading-container');
  //             scope.$watch(attrs.loadingContainer, function(value) {
  //                 loadingLayer.toggleClass('ng-hide', !value);
  //             });
  //         }
  //     };
  // });

  angular.element().ready(function() {
    console.log('was in angular.element().ready');
    angular.resumeBootstrap([app.name]);
  });

});