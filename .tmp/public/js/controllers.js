define(function (require) {

  var angular = require('angular'),
      Controllers = angular.module('controllers', []);

  Controllers.controller('ListCtrl', require('controllers/listCtrl'));
  Controllers.controller('HalftalertCtrl', require('controllers/halftalertCtrl'));
  Controllers.controller('PalertCtrl', require('controllers/palertCtrl'));
  Controllers.controller('PalertDialogCtrl', require('controllers/palertDialogCtrl'));


// angular.module('showcase.withAjax', ['datatables']).controller('WithAjaxCtrl', WithAjaxCtrl);
 
  return Controllers;

});