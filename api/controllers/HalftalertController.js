/**
 * HalftalertController
 *
 * @description :: Server-side logic for managing halftalerts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  index: function (req, res) {
    res.view(null, {
        title: 'HalfTAlert'
    });
  },
  _config: {}
};

