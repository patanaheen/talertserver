/**
 * TalertController
 *
 * @description :: Server-side logic for managing talerts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  index: function (req, res) {
    res.view(null, {
        title: 'List'
    });
  },
  uploadFile: function  (req, res) {
		if(req.method === 'GET')
			return res.json({'status':'GET not allowed'});						
			//	Call to /upload via GET is error
		// console.log(req);
	
		var uploadFile = req.file('uploadFileName');
	    uploadFile.upload({
  				adapter: require('skipper-gridfs'),
  				uri: 'mongodb://localhost:27017/crapDataDB.crapCollection'
				}, function whenDone(err, uploadedFiles) {
					Photo.create({uploadedFileName:uploadedFiles[0].fd,
						sentFileName:uploadedFiles[0].filename,
						photoId:uploadedFiles[0].extra.fileId.toHexString(),
						owner:req.param('yoid')
					}).exec(function (){});
					// Photo.create({uploadedFileName:uploadedFiles[0].extras.fileId,owner:req.param('yoid')}).exec(function (){console.log("success");});
  					if (err) return res.negotiate(err);
  					else return res.ok({
    					files: uploadedFiles,
    					textParams: req.params.all()
  					});
		});
	},
	downloadFile: function (req, res) {
        var blobAdapter = require('skipper-gridfs')({
            uri: 'mongodb://localhost:27017/crapDataDB.crapCollection'
        });

        var fd = req.param('fd'); // value of fd comes here from get request
        var s = req.param('s');
        blobAdapter.read(fd, function(error , file) {
            if(error) {
                res.json(error);
            } else {
                res.contentType('image/png');
                if(s==0){
                	console.log(file)
                	res.send(new Buffer(file));
                }
                else{
                	res.send(new Buffer(file));
                }

            }
        });
    },
  _config: {}
};



// req.file('avatar')
// .upload({
//   adapter: require('skipper-gridfs'),
//   uri: 'mongodb://jimmy@j1mtr0n1xx@mongo.jimmy.com:27017/coolapp.avatar_uploads'
// }, function whenDone(err, uploadedFiles) {
//   if (err) return res.negotiate(err);
//   else return res.ok({
//     files: uploadedFiles,
//     textParams: req.params.all()
//   });
// });