/**
 * PolicealertController
 *
 * @description :: Server-side logic for managing policealerts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  index: function (req, res) {
    res.view(null, {
        title: 'Police Alerts'
    });
  },
  _config: {}
};

