/**
* Policealert.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    "city": {
        "type": "string",
        "required": true
    },
    "location": {
        "type": "string"
    },
    "theft": {
        "type": "boolean",
        "defaultsTo": false
    },
    "harrassment": {
        "type": "boolean",
        "defaultsTo": false
    },
    "murder": {
        "type": "boolean",
        "defaultsTo": false
    },
    "kidnapping": {
        "type": "boolean",
        "defaultsTo": false
    },
    "fraud": {
        "type": "boolean",
        "defaultsTo": false
    },
    "robbery": {
        "type": "boolean",
        "defaultsTo": false
    },
    "creditcardfraud": {
        "type": "boolean",
        "defaultsTo": false
    },
    "verified": {
        "type": "boolean",
        "defaultsTo": false
    },
    "deleted": {
        "type": "boolean",
        "defaultsTo": false
    },
    "userData": {
        "type": "json"
    }
  }
};

