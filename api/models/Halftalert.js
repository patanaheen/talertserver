/**
* Halftalert.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    "city": {
        "type": "string",
        "required": true
    },
    "location": {
        "type": "string"
    },
    "verified": {
        "type": "boolean",
        "defaultsTo": false
    },
    "deleted": {
        "type": "boolean",
        "defaultsTo": false
    },
    "userData": {
        "type": "json"
    }
  }
};

