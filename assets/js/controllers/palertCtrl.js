
define(function () {
  return ['$scope','$http' ,'$filter','ngTableParams','$modal', function($scope, $http,$filter, ngTableParams, $modal) {

      // var Api = $resource('/api/halftalert/find/');
      
      // $scope.tableParams = new ngTableParams({
      //     page: 1,            // show first page
      //     count: 10,          // count per page
      //     sorting: {
      //         name: 'asc'     // initial sorting
      //     }
      // }, {
      //     total: 0,           // length of data
      //     getData: function($defer, params) {
      //         // ajax request to api
      //         Api.get(params.url(), function(data) {
      //             $timeout(function() {
      //                 // update table params
      //                 params.total(data.total);
      //                 // set new data
      //                 $defer.resolve(data.result);
      //             }, 500);
      //         });
      //     }
      // });

    $scope.data = '';

    $scope.addPalert = function (data) {
      console.log("Open Modal fucntion")
      var modalInstance = $modal.open({
        templateUrl: 'myModalContent',
        controller: 'PalertDialogCtrl',
        size: '',
        resolve: {
          data: function () {
            return data;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.tableParams.reload();
        // $scope.palerts.push(data);
        // $scope.selected = selectedItem;
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    };

    $scope.htalertVerify = function (data) {
      $http.put('/api/policealert/update/' + data.id , 
          {verified:((data.verified) ? false: true)}).success(function(){
              $scope.halerts.splice(data.index, 1);
              $scope.tableParams.reload();
          });
    };



      // $scope.addPalert = function () {
     //     ngDialog.open({ template: 'templateId',
     //             className: 'ngDialog-theme-default'});
      // };     

      $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,          // count per page
          sorting: {
              name: 'asc'     // initial sorting
          }
      }, {
          total: 0,           // length of data
          getData: function($defer, params) {
              // ajax request to api

        $http.get('/api/policealert/find?where={"verified":false,"deleted":false}').success(function(data) {
                      params.total(data.length);
                      var orderedData = params.sorting() ? 
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            });
          }
      });
    }];
});
