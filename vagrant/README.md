Summary
-------

The following project is the server side implementation for the t Alert mobile applications. The project was cloned from https://github.com/dgapitts/vagrant-nodejs-angularjs-tutorial.git and will be build on top of the given environment. The base project builds a nice sandbox/training environment for the nodejs and angularjs tutorial (http://docs.angularjs.org/tutorial).

The main features of the base project are that it:

* Builds an Ubuntu i386 (12.04 not final) virtualbox environment (vagrant-nodejs-angularjs) with nodejs and npm pre-installed  
* vagrant-nodejs-angularjs command line includes tree, vim, git, unzip packages plus some "useful aliases"
* All files can be accessed from the host side, enabling usage of more "user friendly" editors like sublime text
* It can also use my regular web-browser via port-forwarding ( http://localhost:4567/ ).

The t Alert server side will be built on MongoDB, Expressjs, Angularjs and Nodejs.